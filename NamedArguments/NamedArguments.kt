package Introduction.NamedArguments

fun joinOptions(options: Collection<String>) =
    options.joinToString(prefix = "[", postfix = "]")